# Security

BirdCalls believes in radical transparency. This repository tracks the history of public documents regarding the security of our service.


- [Security Basics for the Reservation System](https://gitlab.com/birdcalls/documentation/security/-/blob/main/security-basics-reservation-system.md)
