## Account Creation

During account creation, an Account Seed is created for the user, that seed is used to generate a recovery phrase, a Master Key signing pair, and common use signing and encryption keys. The user's username and password are stretched to create login information and a "bootstrap" encryption key.

The derived login information is then passed via SRP to the server along with the encrypted bootstrap file, the public portion of the Master Key, and the public portions of the common use signing and encryption keys to create the account.

Once the account creation process is complete, the private portion of the Master Key is removed from memory, and at no point is the private key ever saved to persistent storage (ie: LocalStorage) or transmitted to the server.

Loss of the user's recovery phrase (the only place the private Master Key is stored) will prevent the user from making certain changes to their accounts, like resetting passwords, changing usernames, etc.

#### Account Seed Generation

> TweetNaCl.js's `nacl.randomBytes()` is called with a length of `32` to create a new 256 bit seed. Before returning the seed, the last byte (8 bits) of the seed is replaced with a `CRC` checksum to ensure BIP39 phrases generated elsewhere will not be recognized as valid BirdCalls recovery phrases. This lowers the entropy of the seed by 8 bits, however since BIP39 only supports 256 bit seeds, we are unable to add an additional checksum on to the 256 bit seed. Including a CRC but limiting it to 8 bits was considered a reasonable tradeoff.

#### Recovery Phrase Generation

The Account Seed is converted using BIP39 to a Recovery Phrase, and the user is asked to record it locally.

> The Account Seed is passed to BIP39's `entropyToMnemonic()` to generate a 24 word recovery phrase, which is shown to the user to be recorded, and the user is asked to verify that they've properly recorded the phrase by verifying random words.

#### Key Generation

Once the user has verified that they have properly recorded the Account Seed, several key pairs are created using the seed and derivation paths.

> Keys are generated from the Account Seed using the Futoin `hkdf()` method, with SHA-512 as the hash function and the derivation path (below) as the `info` field. No salt is passed in, as the recovery phrase needs to reliably generate the same keys without any external information.
>
> The derivation paths used to generate keys from the Account Seed follow the format `/<path-version>/<context>/<index>` where `<path-version>` is currently `0` (but allows for different path formats in the future, `<context>` is the type of key/string to be created, currently either `signing` or `encryption`, and `<index>` is a counter starting at `0`, allowing easy key rotation from the same Account Seed.

A Master Signing Key pair is created from the Account Seed, the public portion of which is used as the account identifier and is passed to the server during account creation. The private portion of the Master Signing Key pair is only ever stored in RAM and is used to verify account modifications (like username or password changes) and to sign new keys when required.

> Master Signing Key pairs always use the index `0` in the derivation path.

A set of "common use" keys is then generated from the Account Seed, using the same method as the Master Key Pair, but with incremented indexes, and are signed by the Master Key to verify that they belong to / are authorized to act on behalf of this account. These keys are used for daily use encryption, signing, etc, and the public portions are passed to the server during account creation.

> "Common use" keys use indexes greater than `0` in their derivation paths.

#### Password Strengthening

To help prevent weak passwords from compromising account security, passwords are passed though PBKDF2 to generate a login secret and a bootstrap encryption key, which dramatically slows down brute force attempts, even with offline data.

> `window.crypto.subtle.deriveBits()` is used with the user's username as a salt and 10,000,000 (10 million) iterations of `PBKDF2`, to generate a 64 byte (512 bit) key. This 512 bit derived key is split into a 256 bit encryption secret (used to encrypt the bootstrap file) and a 256 bit login secret (used in place of the raw password during SRP login).
>
> PBKDF2 was chosen mainly because it's built into the browser's native crypto library, allowing many more rounds to be completed than would be possible using a non native / JS based method. If/when `window.crypto` supports additional algorithms we may update this.
>
> In local testing, 10 million rounds of PBKDF2 can be completed in around 3-10 seconds on modern user devices, and was chosen as a tradeoff between security and usability (the user has to wait every time they enter their password for this process to complete).

#### Bootstrap File

While the Recovery Key is a good way to store the user's Account Seed, and can be used to recover their Master Key and regenerate their Common Keys, it takes significant time to enter and may not be at hand whenever the user wishes to log in. To provide a better user experience, while still maintaining acceptable security, the Bird Calls client stores commonly used information (namely, the "common use" private encryption and signing keys) in an encrypted file that's downloaded whenever the user logs in from a new device. We refer to this as "the bootstrap file". This data is encrypted on the user's own computer, using the bootstrap encryption key (described in the previous section) which never leaves the user's computer.

> The bootstrap file is a JSON encoded string, encrypted with `nacl.secretbox()` using the first 256 bits of the PBKDF2 strengthened password, and a 24 bit randomly generated nonce. The nonce is then prepended to the encrypted data.

The bootstrap file does **NOT** contain the Account Seed or private Master Key, which are never stored and only accessible via the user's recovery phrase.

#### SRP Registration

To prevent even the PBKDF2 strengthened password from being sent to the server, [SRP](https://en.wikipedia.org/wiki/Secure_Remote_Password_protocol) is used to register the account.

> Thinbus's `generateRandomSalt()` function is used to generate a random salt for the SRP registration, and `generateVerifier()`is called with the generated salt, the user's username, and the PBKDF2 strengthened password to generate a verifier. The username, salt, and verifier are then passed to the server, along with the public Master Key, public common signing key, and public common encryption key, to create the new account.

## Bootstrap Retrieval (Login)

The user's bootstrap file contains the information necessary to access Bird Calls resources by authenticating with the server to make requests, as well as the keys necessary to decrypt data stored by users. However, if the user is accessing the service from a new device, or from a device where previously cached data has been removed, the user needs a way to retrieve that bootstrap file without being able to use the keys contained in the file to authenticate their requests.

To accomplish this the user is asked by their local client to provide their username and password, which is passed through the same password strengthening method described in the Account Creation section, providing a 256 bit login secret and a 256 bit encryption secret. The login secret is passed to the SRP client along with the username to authenticate the request and retrieve the bootstrap file. Once retrieved, the client can decrypt the encrypted bootstrap file using the encryption secret generated during the password strengthening step, in order to access the user's common keys.

## Post Login Resource Authentication

General use requests to the server (aside from Account Creation and Bootstrap Retrieval/Login) are authenticated using [HTTP Signatures](https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12) with the user's common use signing key pair, normally cached on the user's device and in the user's bootstrap file.

Bird Calls' implementation follows the HTTP Signatures spec, except that we use ed25519 signatures to sign the requests (as the spec as currently written does not support the signatures used in our chosen crypto library, TweetNaCl.js).

> To secure the request with a signature, a `Digest` header is included that contains a SHA-512 hash of the request contents. The `Signature-Input` header is then generated that includes a `sig1` value of `sig1=(*request-target, *created, host, digest, signature-input);`, and that string is signed with the user's common public signing key using `nacl.sign.detached()`.
>
> The server then retrieves the `keyID` from the `Signature-Input` header, matches it to an existing user, and verifies that the values and signature match in order to authenticate the request.

## Payment Gateway

Since most payment gateways require integrating their own Javascript to function, we've set up all payment related code on a separate subdomain, which communicates with the main birdcalls.app domain using PostMessage calls. The information we send to the payment subdomain is limited, and the payments subdomain has no access to variables stored in the main domain/app (ie: third party payment scripts have no access to keys or any other data stored in memory or local storage).

## Security Related Third Party Libraries

### Client Side (Web App / birdcalls.app)

* TweetNaCl.js <https://github.com/dchest/tweetnacl-js>\
  The core encryption library used to generate secure random data, encrypt, decrypt, sign, and verify. Internally uses `x25519-xsalsa20-poly1305` for public key / asymmetric encryption, `xsalsa20-poly1305` for secret key / symmetric encryption, and `ed25519` for public key signatures. For generating random data, TweetNaCl.js uses `window.crypto.getRandomValues` to generate secure random data.
* Thinbus Javascript Secure Remote Password (SRP) <https://github.com/simbo1905/thinbus-srp-npm>\
  Used to negotiate SRP login with the server without providing the server with the (derived) login password.
* BIP39 <https://github.com/bitcoinjs/bip39>\
  Used to convert between recovery phrases and master key.
* Futoin HKDF <https://github.com/futoin/util-js-hkdf>\
  Used to derive sub-keys from the account's mater key.
* crypto-js <https://github.com/brix/crypto-js>\
  Used for SHA256 and HMAC generation / verification, mostly to hash namespaced strings so that the strings don't collide between users.
* zxcvbn <https://github.com/dropbox/zxcvbn>\
  Used to estimate password strength and prompt the user for tips on creating better passwords. If the user's password does not meet some minimal criteria, they are not permitted to use it.

### Server Side

* Laravel `crypt::*` <https://laravel.com/docs/encryption>\
  To encrypt / decrypt / verify data server side. Internally this library uses `AES-256-CBC` with a MAC for verification.
* Thinbus SRP PHP <https://bitbucket.org/simon_massey/thinbus-php>\
  Used for server side SRP negotiation with the client during login.

## Privacy

### What BirdCalls Knows

The following is a list of all the information the server knows (or can determine) about users registered on the system.

* Username\
  Provided by the user during signup.
* Public Master Key\
  Generated from the user's recovery phrase during signup and used as a global account ID, to sign sub-keys, and to authenticate account change requests (like username/password changes).
* Public Common Signing Key\
  Generated from the user's recovery phrase during signup and used to authenticate API requests and to sign data stored on the server to prove ownership by this account.
* Public Common Encryption Key\
  Generated from the user's recovery phrase during signup and used to verify the integrity of data encrypted by the user (without being able to decrypt it).
* The time, length, and number of calls the user has created.
* The number of devices that joined a particular call, and the times when they joined and left the call.
* Which encrypted files the user has stored on the server (but not the contents of those files).
* IP addresses and device types of visitors to our site (via the web server logs)\
  We do not match these up to user accounts, but technically there is nothing to stop this from happening.
* Payment Information\
  See Payment Gateway Information section below for additional information.

### What the Server Does NOT Know

* Plain Text Passwords.
* Account Seeds.
* Recovery Phrases.
* Private Keys.
* Plain text contents of the bootstrap files.
* Keys used to encrypt the bootstrap files.
* Contents of files users have stored on the server.
* The contents of calls.
* Who owns the devices that joined a call.

### Payment Gateway Information

The payment gateway we use (Stripe) requires certain information to process subscriptions and payments. 

The following information is collected by the payment gateway when creating a payment subscription:

* Email Address\
  Collected so that the gateway can delivery receipts, and to notify the user about free trial completions and upcoming renewals.
* The name on the credit card.
* The credit card number, expiration date, and CVV code.
* The zip or postal code associated with the credit card's billing address.

Stripe provides their customers, BirdCalls included, access to most of this information via the Stripe portal (full credit card number and CVV code are not visible to BirdCalls).

Note that we only pass a randomly generated invoice ID to our payment gateway when taking payment from a user, as we need a way to associate a subscription with a user account, however there is no way for the gateway to use that invoice ID to associate your payment with a specific BirdCalls account.

We have plans to integrate anonymous crypto payments in order to increase anonymity for users who desire it. In the meantime, users who do not want to provide their personal credit card information may be able to use prepaid or gift credit cards to avoid having to enter their real name / postal code.
